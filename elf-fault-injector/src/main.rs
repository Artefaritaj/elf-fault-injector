// @Author: Lashermes Ronan <ronan>
// @Date:   28-06-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 28-06-2017
// @License: MIT


#[macro_use] extern crate clap;
extern crate elf;

pub mod nop;

use clap::{App, ArgMatches};

use std::path::PathBuf;
use std::fs;

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();

    if let Err(e) = run_main(matches) {
        println!("Application error: {}", e);
    }
}

fn run_main(matches: ArgMatches) -> Result<(), String> {

    //get verbosity
    // Vary the output based on how many times the user used the "verbose" flag
    // (i.e. 'myprog -v -v -v' or 'myprog -vvv' vs 'myprog -v'
    //let verbosity = matches.occurrences_of("verbose");

    if let Some(matches) = matches.subcommand_matches("nop") {
        nop_parse(&matches)?;
    }

    Ok(())
}

fn nop_parse(matches: &ArgMatches) -> Result<(), String> {
    let address = if let Some(add) = matches.value_of("address") {
        let removed0x= add.replace("0x", "");
        u64::from_str_radix(&removed0x, 16).map_err(|e|e.to_string())?
    }
    else {
        return Err(format!("An address must be specified."));
    };

    // let word_size = if let Some(size) = matches.value_of("size") {
    //     size.parse::<u8>().map_err(|e|e.to_string())?
    // }
    // else {
    //     return Err(format!("A word size must be specified."));
    // };

    let input_path = if let Some(input) = matches.value_of("input") {
        PathBuf::from(input)
    }
    else {
        return Err(format!("An input file must be specified."));
    };

    let output_path = if let Some(output) = matches.value_of("output") {
        PathBuf::from(output)
    }
    else {
        return Err(format!("An output file must be specified."));
    };

    fs::copy(&input_path, &output_path).map_err(|e|e.to_string())?;

    nop::set_nop(&output_path, address)?;


    Ok(())
}
