// @Author: Lashermes Ronan <ronan>
// @Date:   28-06-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 28-06-2017
// @License: MIT

use std::path::Path;
use std::fs;

use elf::elf_file::ElfFile;

fn find_section(address: u64, elf_file: &ElfFile) -> Result<String, String> {
    for sec in elf_file.sections.iter() {
        let section_name = &sec.name;
        let start_add = sec.shdr.addr;
        let size = sec.shdr.size;
        let end_add = start_add + size;
        //println!("Section {} found: [0x{:x} -> 0x{:x}[", section_name, start_add, end_add);

        if address >= start_add && address < end_add {
            return Ok(section_name.to_string());
        }
    }
    return Err(format!("0x{:x} has not been found in the elf.", address));
}

pub fn set_nop(file: &Path, address: u64) -> Result<ElfFile, String> {
    let mut elf_file = ElfFile::open_path(file).map_err(|_|format!("Cannot open and parse elf file."))?;
    let data = elf_file.ehdr.data;

    println!("File {} opened.", file.display());
    let selected_name = find_section(address, &elf_file)?;
    println!("Selected address is in section {}.", selected_name);

    {
        let selected = elf_file.get_section_mut(selected_name).ok_or(format!("Section cannot be selected"))?;
        // let index = address - selected.shdr.addr;
        // let val_test = selected.data[index as usize];
        // println!("Byte read before @ 0x{:08x} = 0x{:02x}", address, val_test);

        //replace instruction with nop
        selected.replace_word(data, address, 2, 0xbf00)?;

        let mut io_file = fs::OpenOptions::new().write(true).open(file).map_err(|e|format!("Cannot open output file: {}", e.to_string()))?;
        selected.write_data(&mut io_file).map_err(|e|format!("Cannot write data: {}", e.to_string()))?;

        // let val_test = selected.data[index as usize];
        // println!("Byte read after @ 0x{:08x} = 0x{:02x}", address, val_test);
    }

    Ok(elf_file)
}
